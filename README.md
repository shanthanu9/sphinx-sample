# Sphinx sample

This repo is created to understand how to document a python project using 
[sphinx](https://www.sphinx-doc.org/en/master/).

## Repo tour

### sampleproj

[sampleproj](sampleproj) contains a sample python package which implements 4
methods namely: `add`, `sub`, `mul` and `div`.

Each of these methods are defined in
[sampleproj/operator.py](sampleproj/operator.py) with [numpy
style](https://numpydoc.readthedocs.io/en/latest/example.html) doc strings 
documenting their usage.

[sample.py](sample.py) contains an example usage of this package.

### docs

As the name suggests, this directory is related to documentation. This is the
root folder for sphinx tool.

Refer [docs/README.md](docs/README.md) for steps to generate sphinx 
documentation.