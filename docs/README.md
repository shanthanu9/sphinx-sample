# docs

This is the source directory of sphinx tool. I will document below all the steps
I followed for creating this folder (for my future self):

## Document generation

Ensure that you are in the current directory ([docs](.)) before running the
below commands.

#### 1. Create a virtual environment in this direcotry named `venv`
```
$ python3 -m venv venv
$ source venv/bin/activate
```

#### 2. Install all required packages
```
$ pip install -r doc_requirements.txt
```

#### 3. Generate the html document
```
$ make html
```

#### 4. View the html document created
Open `_build/html/index.html` in your favorite browser.

## Initial setup

These were the steps I initially followed for creating this directory. 
This needs to be done only for the **first** time.

#### 1. Create a docs directory
```
$ mkdir docs
$ cd docs
```

#### 2. Create a virtual environment in this direcotry named `venv`
```
$ python3 -m venv venv
$ source venv/bin/activate
```

#### 3. Install sphinx
```
pip install sphinx
```

#### 4. Quick start sphinx
```
$ sphinx-quickstart
```
When run, this command will prompt to enter some config values. I mostly used
the default values here. Certain values asked here can be later changed in
`conf.py`.

After running this command, following files will be generated:
```
_build/  conf.py  index.rst  make.bat  Makefile  README.md  _static/  _templates/
```

#### 5. Make changes in [`conf.py`](conf.py). Following are the main changes made:
- Change default theme to [sphinx_rtd_theme](https://github.com/readthedocs/sphinx_rtd_theme)
- Use [sphinx.ext.autodoc](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html)
for generating documentation from python docstrings.
- Use for
    [sphinx.ext.napoleon](https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html#module-sphinx.ext.napoleon)
    understanding numpy style docstrings.

#### 6. Make changes in [`index.rst`](index.rst)
Add reference to auto generated API documentation for [`sampleproj`](../sampleproj).

#### 7. Keep track of all python packages needed by sphinx
```
$ pip freeze > doc_requirements.txt
```