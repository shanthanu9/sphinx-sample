"""
operator.py
===========
Defines important parameters.
"""

def add(a, b):
    """Sum of two numbers

    Parameters
    ----------
    a : float
        First parameter
    b : float
        Second parameter

    Returns
    -------
    float
        Sum of parameters

    Examples
    --------
    
    >>> sum(2, 3)
    5
    """
    return a+b

def sub(a, b):
    """Difference of two numbers

    Parameters
    ----------
    a : float
        First parameter
    b : float
        Second parameter

    Returns
    -------
    float
        Difference of parameters

    Examples
    --------
    
    >>> sub(2, 3)
    -1
    """
    return a-b

def mul(a, b):
    """Product of two numbers

    Parameters
    ----------
    a : float
        First parameter
    b : float
        Second parameter

    Returns
    -------
    float
        Product of parameters

    Examples
    --------
    
    >>> mul(2, 3)
    6
    """
    return a*b

def div(a, b):
    """Quotiant of two numbers

    Parameters
    ----------
    a : float
        First parameter
    b : float
        Second parameter

    Returns
    -------
    float
        Quotiant of parameters

    Examples
    --------
    
    >>> div(4, 2)
    2
    """
    return a/b